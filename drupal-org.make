api = 2
core = 7.x

projects[calendar][version] = "3.4"
projects[calendar][subdir] = contrib

projects[context][version] = "3.1"
projects[context][subdir] = contrib

projects[ctools][version] = "1.3"
projects[ctools][subdir] = contrib

projects[date][version] = "2.7"
projects[date][subdir] = contrib

projects[date_ical][version] = "2.12"
projects[date_ical][subdir] = contrib

projects[ds][version] = "2.6"
projects[ds][subdir] = contrib

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = contrib

projects[entity_view_mode][version] = "1.0-rc1"
projects[entity_view_mode][subdir] = contrib

projects[features][version] = "1.0"
projects[features][subdir] = contrib

projects[entity][version] = "1.3"
projects[entity][subdir] = contrib

projects[panels][version] = "3.3"
projects[panels][subdir] = contrib

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = contrib

projects[libraries][version] = "2.1"
projects[libraries][subdir] = contrib

projects[markdown][version] = "1.2"
projects[markdown][subdir] = "contrib"

projects[metatag][version] = "1.0-beta9"
projects[metatag][subdir] = "contrib"

projects[omega][version] = "3.1"
projects[omega][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = contrib

projects[token][version] = "1.5"
projects[token][subdir] = contrib

projects[profile2][version] = "1.3"
projects[profile2][subdir] = contrib

projects[views][version] = "3.7"
projects[views][subdir] = contrib

