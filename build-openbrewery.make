api = 2
core = 7.x

includes[] drupal-org-core.make

projects[openbrewery][version] = 7.x-1.x
projects[openbrewery][download][type] = git
projects[openbrewery][download][url] = http://git.drupal.org/project/openbrewery.git
projects[openbrewery][download][branch] = 7.x-1.x
projects[openbrewery][type] = profile

