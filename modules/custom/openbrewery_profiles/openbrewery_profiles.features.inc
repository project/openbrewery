<?php
/**
 * @file
 * openbrewery_profiles.features.inc
 */

/**
 * Implements hook_views_api().
 */
function openbrewery_profiles_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_default_profile2_type().
 */
function openbrewery_profiles_default_profile2_type() {
  $items = array();
  $items['openbrewery_employee'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "openbrewery_employee",
    "label" : "Profile",
    "weight" : "0",
    "data" : { "registration" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}
