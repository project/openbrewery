<?php
/**
 * @file
 * openbrewery_profiles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function openbrewery_profiles_user_default_roles() {
  $roles = array();

  // Exported role: Employee.
  $roles['Employee'] = array(
    'name' => 'Employee',
    'weight' => '3',
  );

  return $roles;
}
