<?php

function openbrewery_age_verify_settings_form($form, &$form_state) {
  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Openbrewery Age Verify settings'),
    '#description' => t('Settings for the Openbrewery Age Verify module.')
  );

  $age_range = range(1, 120);
  $form['settings']['openbrewery_verify_age'] = array(
    '#type' => 'select',
    '#title' => t('Age limit'),
    '#options' => array_combine($age_range, $age_range),
    '#default_value' => variable_get('openbrewery_verify_age', 0),
  );

  $expires_range = range(1, 31);
  $form['settings']['openbrewery_verify_cookie_expire'] = array(
    '#type' => 'select',
    '#title' => t('Days before the cookie expires'),
    '#options' => array_combine($expires_range, $expires_range),
    '#default_value' => variable_get('openbrewery_verify_cookie_expire', 0),
  );

  $form['settings']['openbrewery_verify_underage_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Underage message'),
    '#description' => t('Message that is displayed to underage users.'),
    '#default_value' => variable_get('openbrewery_verify_underage_message', ''),
  );

  return system_settings_form($form);
}

