<?php
/**
 * @file
 * openbrewery_beers.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function openbrewery_beers_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openbrewery_beers';
  $view->description = 'A view that shows all of the years, grouped by availability';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Openbrewery Beers';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Our Beer';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_beer_abv' => 'field_beer_abv',
    'field_beer_availability' => 'field_beer_availability',
    'body' => 'body',
    'field_beer_ibus' => 'field_beer_ibus',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_beer_abv' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_beer_availability' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_beer_ibus' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = 'In facilisis, mauris quis eleifend fringilla, leo odio tempus tortor, et ultricies tellus est at leo. Phasellus molestie fermentum malesuada. Maecenas tincidunt eleifend congue. Nullam sed elit nec neque varius pharetra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce dui nisi, accumsan quis auctor lobortis, fermentum vel neque. Nam sed arcu leo. Maecenas malesuada dui quis est hendrerit non volutpat dui ultricies. Fusce in risus vel lectus gravida porta. Aenean et dolor in neque fringilla luctus. Pellentesque vestibulum, urna fermentum pulvinar cursus, felis lectus vehicula neque, vitae porta erat massa id lorem. Fusce sit amet nunc et massa mattis bibendum vitae non turpis. Duis at eros mauris. Etiam lacus diam, pretium in luctus nec, tincidunt eget odio. Curabitur ullamcorper, dolor eget gravida sagittis, nulla tellus bibendum diam, sit amet vestibulum turpis neque ac ligula. Aliquam urna dolor, euismod quis vestibulum vel, consequat eu eros.
';
  $handler->display->display_options['header']['area']['format'] = 'markdown';
  /* Field: Content: Photo */
  $handler->display->display_options['fields']['field_beer_photo']['id'] = 'field_beer_photo';
  $handler->display->display_options['fields']['field_beer_photo']['table'] = 'field_data_field_beer_photo';
  $handler->display->display_options['fields']['field_beer_photo']['field'] = 'field_beer_photo';
  $handler->display->display_options['fields']['field_beer_photo']['label'] = '';
  $handler->display->display_options['fields']['field_beer_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_beer_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_beer_photo']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: ABV */
  $handler->display->display_options['fields']['field_beer_abv']['id'] = 'field_beer_abv';
  $handler->display->display_options['fields']['field_beer_abv']['table'] = 'field_data_field_beer_abv';
  $handler->display->display_options['fields']['field_beer_abv']['field'] = 'field_beer_abv';
  $handler->display->display_options['fields']['field_beer_abv']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: IBUs */
  $handler->display->display_options['fields']['field_beer_ibus']['id'] = 'field_beer_ibus';
  $handler->display->display_options['fields']['field_beer_ibus']['table'] = 'field_data_field_beer_ibus';
  $handler->display->display_options['fields']['field_beer_ibus']['field'] = 'field_beer_ibus';
  $handler->display->display_options['fields']['field_beer_ibus']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Availability */
  $handler->display->display_options['fields']['field_beer_availability']['id'] = 'field_beer_availability';
  $handler->display->display_options['fields']['field_beer_availability']['table'] = 'field_data_field_beer_availability';
  $handler->display->display_options['fields']['field_beer_availability']['field'] = 'field_beer_availability';
  $handler->display->display_options['fields']['field_beer_availability']['delta_offset'] = '0';
  /* Field: Content: Beer Status */
  $handler->display->display_options['fields']['field_beer_status']['id'] = 'field_beer_status';
  $handler->display->display_options['fields']['field_beer_status']['table'] = 'field_data_field_beer_status';
  $handler->display->display_options['fields']['field_beer_status']['field'] = 'field_beer_status';
  $handler->display->display_options['fields']['field_beer_status']['label'] = 'Status';
  $handler->display->display_options['fields']['field_beer_status']['delta_offset'] = '0';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'openbrewery_beer' => 'openbrewery_beer',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'beer';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Our Beer';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['openbrewery_beers'] = $view;

  return $export;
}
