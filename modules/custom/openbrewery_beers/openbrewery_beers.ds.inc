<?php
/**
 * @file
 * openbrewery_beers.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function openbrewery_beers_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|openbrewery_beer|beer_view';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'openbrewery_beer';
  $ds_fieldsetting->view_mode = 'beer_view';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '-1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|openbrewery_beer|beer_view'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function openbrewery_beers_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|openbrewery_beer|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'openbrewery_beer';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'body',
      ),
      'right' => array(
        1 => 'field_beer_photo',
        2 => 'field_beer_abv',
        3 => 'field_beer_ibus',
        4 => 'field_beer_style',
        5 => 'field_beer_availability',
        6 => 'field_beer_packaging',
        7 => 'field_beer_status',
      ),
    ),
    'fields' => array(
      'body' => 'left',
      'field_beer_photo' => 'right',
      'field_beer_abv' => 'right',
      'field_beer_ibus' => 'right',
      'field_beer_style' => 'right',
      'field_beer_availability' => 'right',
      'field_beer_packaging' => 'right',
      'field_beer_status' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|openbrewery_beer|default'] = $ds_layout;

  return $export;
}
